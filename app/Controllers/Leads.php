<?php namespace App\Controllers;
use CodeIgniter\RESTful\ResourceController;
use App\Models\LeadsModel;


class Leads extends ResourceController
{
	protected $format = 'json';

	public function __construct() {
		$this->model = new LeadsModel();
	}

	// Handles GET Request (leads)
    	public function index(){
		$data['leads'] = $this->model->orderBy('id', 'DESC')->findAll();
		return $this->respond($data);
	}

	// Handles GET Request (leads/(:num) OR (:segment))
	public function show($id = null) {
		return $this->respond($this->model->find($id));
	}

	// Handles POST Request (leads)
	public function create() {
		$data = $this->request->getPost();
		log_message("debug", print_r($data, TRUE));
		$insert_id = $this->model->insert($data);
		if(is_numeric($insert_id)) {
			return $this->respond(array("result"=>"success", "lead_id" => $insert_id));
		} else {
			return $this->respond(array("result"=>"error"));
		}
	}

}
?>
