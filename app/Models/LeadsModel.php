<?php 
namespace App\Models;
use CodeIgniter\Model;

class LeadsModel extends Model {

	protected $table = 'leads';
	protected $primaryKey = 'id';
	protected $allowedFields = [
		'client',
		'fname', 
		'lname', 
		'phone',
		'email',
		'addr1',
		'addr2',
		'city',
		'state',
		'zip',
		'gender',
		'dob',
		'medicare_id',
		'secondary',
		'secondary_id',
		'secondary_year',
		'item',
		'type',
		'size',
		'item_num',
		'upload_date',
		'trusted_form',
		'group_code',
		'source_name',
		'campaign_type',
		'lead_source_code',
		'lead_source_name'	
	];
}
